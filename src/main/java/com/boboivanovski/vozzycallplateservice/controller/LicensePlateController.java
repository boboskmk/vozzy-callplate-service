package com.boboivanovski.vozzycallplateservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.boboivanovski.vozzycallplateservice.model.api.request.LicensePlateRequest;
import com.boboivanovski.vozzycallplateservice.model.api.request.NewBlockingRequest;
import com.boboivanovski.vozzycallplateservice.model.api.response.GenericResponse;
import com.boboivanovski.vozzycallplateservice.service.LicensePlateService;

@RestController
public class LicensePlateController {

	@Autowired
	LicensePlateService licensePlateService;
	
	@PostMapping("/request")
	public ResponseEntity<GenericResponse> newBlockingRequest(@RequestBody NewBlockingRequest request) {
		return ResponseEntity.ok(licensePlateService.sendBlockingRequest(request));
	}
	
	@PostMapping("/license-plate")
	public ResponseEntity<GenericResponse> addLicensePlate(@RequestBody LicensePlateRequest request) {
		return ResponseEntity.ok(licensePlateService.addLicensePlate(request));
	} 
	
	@DeleteMapping("/license-plate")
	public ResponseEntity<GenericResponse> removeLicensePlate(@RequestBody LicensePlateRequest request) {
		return ResponseEntity.ok(licensePlateService.removeLicensePlate(request));
	} 
	
}
