package com.boboivanovski.vozzycallplateservice.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.boboivanovski.vozzycallplateservice.model.api.request.LicensePlateRequest;

@Document(collection = "license_plate")
public class LicensePlate {
	
	@Id
	private String id;
	
	private String plateNumber;
	
	private Country country;
	
	private List<String> userIds;
	
	
	public LicensePlate() {
		
	}


	public LicensePlate(LicensePlateRequest licensePlateRequest) {
		new LicensePlate(licensePlateRequest.getLicensePlate().getPlateNumber(), licensePlateRequest.getLicensePlate().getCountry(), licensePlateRequest.getUserId());
	}

	public LicensePlate(String plateNumber, Country country, String userId) {
		this.plateNumber = plateNumber;
		this.country = country;
		this.userIds = new ArrayList<String>();
		this.userIds.add(userId);
	}

	public LicensePlate(String plateNumber, Country country, List<String> userIds) {
		this.plateNumber = plateNumber;
		this.country = country;
		this.userIds = userIds;
	}

	public String getPlateNumber() {
		return plateNumber;
	}


	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}


	public Country getCountry() {
		return country;
	}


	public void setCountry(Country country) {
		this.country = country;
	}

	public void addUserId(String userId) {
		if (this.userIds == null) {
			this.userIds = new ArrayList<String>();
		}
		this.userIds.add(userId);
	}
	
	public void removeUserId(String userId) {
		if (this.userIds != null) {
			this.userIds.remove(userId);
		}
	}
	
	


	public List<String> getUserIds() {
		return userIds;
	}



	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}



	public String getId() {
		return id;
	}
	
	
	

}
