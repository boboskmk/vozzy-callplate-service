package com.boboivanovski.vozzycallplateservice.model.api.request;

import org.springframework.lang.Nullable;

public class NewBlockingRequest {
	
	private String plateNumber;
	
	@Nullable
	private String countryName;
	
	/// sender
	private String userId;
	
	public NewBlockingRequest(){
		
	}

	public NewBlockingRequest(String plateNumber, String countryName, String userId) {
		this.plateNumber = plateNumber;
		this.countryName = countryName;
		this.userId = userId;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public String getCountryName() {
		return countryName;
	}

	public String getUserId() {
		return userId;
	}
	
	
	
	

}
