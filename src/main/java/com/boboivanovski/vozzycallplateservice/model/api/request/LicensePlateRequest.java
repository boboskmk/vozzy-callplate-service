package com.boboivanovski.vozzycallplateservice.model.api.request;

import com.boboivanovski.vozzycallplateservice.model.LicensePlate;

public class LicensePlateRequest {
	
	private String userId;
	
	private LicensePlate licensePlate;
	
	public LicensePlateRequest() {
		
	}

	public LicensePlateRequest(String userId, LicensePlate licensePlate) {
		this.userId = userId;
		this.licensePlate = licensePlate;
	}

	public String getUserId() {
		return userId;
	}

	public LicensePlate getLicensePlate() {
		return licensePlate;
	}
	
	
	
	

}
