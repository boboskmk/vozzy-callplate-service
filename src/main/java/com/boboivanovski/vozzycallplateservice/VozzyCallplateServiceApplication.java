package com.boboivanovski.vozzycallplateservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VozzyCallplateServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(VozzyCallplateServiceApplication.class, args);
	}

}
