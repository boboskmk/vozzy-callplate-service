package com.boboivanovski.vozzycallplateservice.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.boboivanovski.vozzycallplateservice.model.LicensePlate;

public interface LicensePlateRepository  extends MongoRepository<LicensePlate, String>  {

	Optional<LicensePlate> findByPlateNumberAndCountryName(String plateNumber, String name);
}
