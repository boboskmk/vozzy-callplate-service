package com.boboivanovski.vozzycallplateservice.service;

import com.boboivanovski.vozzycallplateservice.model.api.request.LicensePlateRequest;
import com.boboivanovski.vozzycallplateservice.model.api.request.NewBlockingRequest;
import com.boboivanovski.vozzycallplateservice.model.api.response.GenericResponse;

public interface LicensePlateService {
	
	GenericResponse sendBlockingRequest(NewBlockingRequest request);
	
	GenericResponse respondToBlockingRequest();
	
	GenericResponse addLicensePlate(LicensePlateRequest request);
	
	GenericResponse removeLicensePlate(LicensePlateRequest request);
	
	// send message
	
	// leave from chat
	
	// 

}
