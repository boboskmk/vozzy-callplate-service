package com.boboivanovski.vozzycallplateservice.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.boboivanovski.vozzycallplateservice.model.LicensePlate;
import com.boboivanovski.vozzycallplateservice.model.api.request.LicensePlateRequest;
import com.boboivanovski.vozzycallplateservice.model.api.request.NewBlockingRequest;
import com.boboivanovski.vozzycallplateservice.model.api.response.GenericResponse;
import com.boboivanovski.vozzycallplateservice.repository.LicensePlateRepository;
import com.boboivanovski.vozzycallplateservice.service.LicensePlateService;
import com.boboivanovski.vozzyuserservice.exceptions.BadRequestException;
import com.boboivanovski.vozzyuserservice.exceptions.ConflictException;
import com.boboivanovski.vozzyuserservice.exceptions.RecordNotFoundException;


@Service
public class LicensePlateServiceImpl implements LicensePlateService {
	
	@Autowired
	LicensePlateRepository licensePlateRepository;

	@Override
	public GenericResponse sendBlockingRequest(NewBlockingRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse respondToBlockingRequest() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GenericResponse addLicensePlate(LicensePlateRequest request) {
		
		checkLicensePlateRequest(request);
		
		Optional<LicensePlate> licensePlateOpt = licensePlateRepository.findByPlateNumberAndCountryName(request.getLicensePlate().getPlateNumber(), request.getLicensePlate().getCountry().getName());
		
		if (licensePlateOpt.isEmpty()) {
			throw new RecordNotFoundException("License plate does not exists");
		}
		
		
		final LicensePlate licensePlate = licensePlateOpt.get();

		// if only one user uses this license-plate and that one is removing the license plate, then delete the whole object
		if (licensePlate.getUserIds().size() == 1) {
			
			this.licensePlateRepository.delete(licensePlate);
			
			return new GenericResponse(HttpStatus.OK, "License plate was successfully added");
			
		} else { // else just remove the user id from the object
			
			final String userId = request.getUserId();
			
			if (licensePlate.getUserIds().contains(userId)) {
				licensePlate.removeUserId(userId);
				this.licensePlateRepository.save(licensePlate);
			}

			return new GenericResponse(HttpStatus.OK, "License plate was successfully added");
		}
	}

	@Override
	public GenericResponse removeLicensePlate(LicensePlateRequest request) {
		
		checkLicensePlateRequest(request);

		Optional<LicensePlate> licensePlateOpt = licensePlateRepository.findByPlateNumberAndCountryName(request.getLicensePlate().getPlateNumber(), request.getLicensePlate().getCountry().getName());
		
		if (licensePlateOpt.isPresent()) {
			
			final LicensePlate licensePlate = licensePlateOpt.get();
			
			if (!licensePlate.getUserIds().contains(request.getUserId())) {
				licensePlate.addUserId(request.getUserId());
				
				this.licensePlateRepository.save(licensePlate);
			}
			
			return new GenericResponse(HttpStatus.OK, "License plate was successfully added");
			
		} else {
			
			final LicensePlate licensePlate = new LicensePlate(request);
			
			this.licensePlateRepository.insert(licensePlate);
			
			return new GenericResponse(HttpStatus.OK, "License plate was successfully added");
			
		}
	}

	private void checkLicensePlateRequest(LicensePlateRequest request) {
		if (request.getLicensePlate() == null) {
			throw new BadRequestException("License plate cannot be empty");
		}
		
		if (request.getUserId() == null || request.getUserId().isEmpty()) {
			throw new BadRequestException("User ID cannot be empty");
		}
	}    
}
